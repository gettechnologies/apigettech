import { createPool } from "mysql2/promise";
import {
    DB_HOST,
    DB_USER,
    DB_PASS,
    DB_PORT,
    DB_NAME
} from "./config.js";

export const pool = createPool({
    host: DB_HOST,
    user: DB_USER,
    database: DB_NAME,
    password: DB_PASS,
    port: DB_PORT,
    waitForConnections: true,
    connectionLimit: 20,
    queueLimit: 0,
    idleTimeout:60000,  
});