export const validateString = (cadena) => {
	try {
		let regex = new RegExp('^[a-zA-Z0-9\\s.,áéíóúÁÉÍÓÚñÑ-]+$');
		return regex.test(cadena);	//Retorna 'true' si no contiene caracteres especiales
	} catch (error) {
		console.log(error);
	}
}


export const validateFecha = (fecha) => {
	try{
		const regex = /^\d{4}-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])$/;
		return regex.test(fecha);
	}catch(error){
		console.log(error);
	}
}

export const validateData = (data) => {
	if(Object.keys(data).length === 0){
		return true
	}
	return false
}