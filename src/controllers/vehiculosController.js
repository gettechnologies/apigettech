import { pool } from "../connection.js";
import { validateData} from "../validation.js";
import {
    getAllData
} from "../models/get_model.js";
import { API_KEY_ADMIN, API_KEY_USER} from "../app.js";



//* Funciones de obtencion
export const getVehiculos = async (req, res) => {
    try{
        if(validateData(req.body)){
            return res.status(400).json({
                message: "Parametros no recibidos"
            });
        }

        let {key} = req.body;

        //! Validación de la Key
        if(key == null){
            return res.status(400).json({
                message: "Parametros no validos"
            });
        }

        key = key.toString();

        if(!(key == API_KEY_ADMIN)){
            return res.status(403).json({
                message: "Acceso no autorizado"
            });
        }
        //! Fin Validación de la Key

        const rows = await getAllData('Vehiculos');
        if(rows.length <= 0){
            return res.status(404).json({
                message: "No se encontraron datos"
            });
        }
        res.status(200).json(rows);
    }catch(error){
        return res.status(500).json({
            message : "Error algo salio mal"
        });
    }
}



export const getVehiculo = async (req, res) => {
    try {
        if(validateData(req.body)){
            return res.status(400).json({
                message: "Parametros no recibidos"
            });
        }

        let {key} = req.body;
        let {id} = req.params;

        if(isNaN(id)){
            return res.status(400).json({
                message: "EL id debe ser númerico"
            });
        }
        id = parseInt(id);

        //! Validación de la Key
        if(key == null){
            return res.status(400).json({
                message: "Parametros no validos"
            });
        }

        key = key.toString();

        if(!(key == API_KEY_ADMIN)){
            return res.status(403).json({
                message: "Acceso no autorizado"
            });
        }
        //! Fin Validación de la Key

        const [rows] = await pool.query('SELECT * FROM Vehiculos WHERE id = ?', [id]);

        if(rows.length <= 0){
            return res.status(404).json({
                message: "Vehiculo no encontrada"
            });
        }

        res.status(202).json(rows)
    }catch(error){
        return res.status(500).json({
            message : "Error algo salio mal"
        });
    }
}

//* Funciones de creación o actualización
export const createVehiculo = async (req, res) => {
    try{
        if(validateData(req.body)){
            return res.status(400).json({
                message: "Parametros no recibidos"
            });
        }

        let {placa, tipo_vehiculo_id, key} = req.body;
        if(placa == null || tipo_vehiculo_id == null || key == null){
            return res.status(400).json({
                message: "Parametros no validos."
            });
        }

        key = key.toString();

        //! Validación de la Key
        if(!(key == API_KEY_ADMIN)){
            return res.status(403).json({
                message: "Acceso no autorizado"
            });
        }
        //! Fin Validación de la Key

        
        //! Fin de validaciones

        //* Se envían los datos
        const [rows] = await pool.query('INSERT INTO Vehiculos(placa, tipo_vehiculo_id) VALUES (?, ?)', [placa, tipo_vehiculo_id]);
        
        
        if(rows.affectedRows === 0){
            return res.status(404).json({
                message: "Error al realizar la inserción"
            });
        }
        else{
            if(tipo_vehiculo_id==2){
                const [rowsRes] = await pool.query('INSERT INTO PagosResidentes(vehiculo_id) VALUES (?)', [rows.insertId]);
            }
            res.status(201).json({
            message: "Vehiculo creado con exito.",
            id: rows.insertId
        })
        }

        
    }catch(error){
        console.log(error);
        return res.status(500).json({
            message: "Error algo salio mal"
        });
    }
}

export const updateVehiculo = async (req, res) => {
    try{
        if(validateData(req.body)){
            return res.status(400).json({
                message: "Parametros no recibidos. El recurso no se actualizo"
            });
        }

        let {placa, tipo_vehiculo_id, key} = req.body;
        if(placa == null || tipo_vehiculo_id == null || key == null){
            return res.status(400).json({
                message: "Parametros no validos."
            });
        }
        let {id} = req.params;

        if(isNaN(id)){
            return res.status(400).json({
                message: "EL id no es númerico"
            });
        }
        id = parseInt(id);

        //! Validación de la Key
        if(key == null){
            return res.status(400).json({
                message: "Parametros no validos. El recurso no se actualizo"
            });
        }
        key = key.toString();
        if(!(key == API_KEY_ADMIN)){
            return res.status(403).json({
                message: "Acceso no autorizado"
            });
        }
        //! Fin Validación de la Key

        
        //! Fin de validaciones

        //* Se realiza el envio de los datos
        const [result] = await pool.query('UPDATE vehiculos SET placa = IFNULL(?,placa), tipo_vehiculo_id = IFNULL(?,tipo_vehiculo_id) WHERE id = ?',
        [placa, tipo_vehiculo_id,id]);

        if(result.affectedRows === 0){
            return res.status(404).json({
                message: "Vehiculo no encontrado."
            });
        }

        res.status(200).json({
            message: "Se actualizo correctamente"
        })
    }catch(error){
        console.log(error);
        return res.status(500).json({
            message : "Error algo salio mal"
        });
    }
}