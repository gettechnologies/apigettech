import { pool } from "../connection.js";
import { validateData, validateString } from "../validation.js";

import { API_KEY_ADMIN, API_KEY_USER} from "../app.js";

export const createTipoVehiculo = async (req, res) => {
    const { descripcion, key } = req.body;
    if (!descripcion || !key || !(key === API_KEY_ADMIN)) {
        return res.status(400).json({ message: "Parámetros no válidos o acceso no autorizado." });
    }

    try {
        const result = await pool.query('INSERT INTO TiposVehiculos (descripcion) VALUES (?)', [descripcion]);
        if (result.affectedRows === 0) {
            return res.status(404).json({ message: "Error al crear el tipo de vehículo." });
        }
        res.status(201).json({ message: "Tipo de vehículo creado con éxito.", id: result.insertId });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: "Error al crear el tipo de vehículo." });
    }
};

export const getTiposVehiculos = async (req, res) => {
    const { key } = req.body;
    if (!key || !(key === API_KEY_ADMIN)) {
        return res.status(403).json({ message: "Acceso no autorizado." });
    }

    try {
        const [rows] = await pool.query('SELECT * FROM TiposVehiculos');
        res.status(200).json(rows);
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: "Error al obtener los tipos de vehículos." });
    }
};

export const updateTipoVehiculo = async (req, res) => {
    const { id } = req.params;
    const { descripcion, key } = req.body;
    if (!descripcion || !key || !(key === API_KEY_ADMIN)) {
        return res.status(400).json({ message: "Parámetros no válidos o acceso no autorizado." });
    }

    try {
        const result = await pool.query('UPDATE TiposVehiculos SET descripcion = ? WHERE id = ?', [descripcion, id]);
        if (result.affectedRows === 0) {
            return res.status(404).json({ message: "Tipo de vehículo no encontrado." });
        }
        res.status(200).json({ message: "Tipo de vehículo actualizado con éxito." });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: "Error al actualizar el tipo de vehículo." });
    }
};

export const deleteTipoVehiculo = async (req, res) => {
    const { id } = req.params;
    const { key } = req.body;
    if (!key || !(key === API_KEY_ADMIN)) {
        return res.status(403).json({ message: "Acceso no autorizado." });
    }

    try {
        const result = await pool.query('DELETE FROM TiposVehiculos WHERE id = ?', [id]);
        if (result.affectedRows === 0) {
            return res.status(404).json({ message: "Tipo de vehículo no encontrado." });
        }
        res.status(200).json({ message: "Tipo de vehículo eliminado con éxito." });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: "Error al eliminar el tipo de vehículo." });
    }
};
