import { pool } from "../connection.js";
import { validateData, validateString } from "../validation.js";

import { API_KEY_ADMIN, API_KEY_USER} from "../app.js";

// Función para crear una nueva estancia
export const createEstancia = async (req, res) => {
    try {
        const { vehiculo_id, hora_entrada, key } = req.body;
        if (!vehiculo_id ||  !key || !(key == API_KEY_ADMIN)) {
            return res.status(400).json({ message: "Parámetros no válidos o acceso no autorizado." });
        }

        const result = await pool.query('INSERT INTO Estancias (vehiculo_id, hora_entrada) VALUES (?, current_timestamp())', [vehiculo_id]);

        if (result.affectedRows === 0) {
            return res.status(404).json({ message: "Error al crear la estancia." });
        }

        res.status(201).json({ message: "Estancia creada con éxito.", id: result.insertId });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: "Error al crear la estancia." });
    }
};

// Función para obtener todas las estancias
export const getEstancias = async (req, res) => {
    try {
        const { key } = req.body;
        if (!key || !(key == API_KEY_ADMIN)) {
            return res.status(403).json({ message: "Acceso no autorizado." });
        }

        const [rows] = await pool.query('SELECT * FROM Estancias');

        if (rows.length <= 0) {
            return res.status(404).json({ message: "No se encontraron estancias." });
        }

        res.status(200).json(rows);
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: "Error al obtener las estancias." });
    }
};

// Función para actualizar una estancia
export const updateEstancia = async (req, res) => {
    const { id } = req.params;
    const { key } = req.body;
    const costoPorMinuto = 0.5; // Define el costo por minuto aquí

    if (!key || !(key == API_KEY_ADMIN)) {
        return res.status(403).json({ message: "Acceso no autorizado." });
    }

    try {
        // Paso 1: Determinar si el vehículo es de tipo residente
        const [vehiculoRows] = await pool.query(`
            SELECT Vehiculos.tipo_vehiculo_id
            FROM Estancias
            INNER JOIN Vehiculos ON Estancias.vehiculo_id = Vehiculos.id
            WHERE Estancias.id = ?`, [id]);

        if (vehiculoRows.length === 0) {
            return res.status(404).json({ message: "Estancia no encontrada." });
        }

        const esResidente = vehiculoRows[0].tipo_vehiculo_id === 2;

        // Paso 2: Actualizar estancia y calcular total a pagar
        const [result] = await pool.query(`
            UPDATE Estancias 
            SET hora_salida = CURRENT_TIMESTAMP,
                total_a_pagar = TIMESTAMPDIFF(MINUTE, hora_entrada, CURRENT_TIMESTAMP) * ?
            WHERE id = ?`, [costoPorMinuto, id]);

        if (result.affectedRows === 0) {
            return res.status(404).json({ message: "Estancia no encontrada." });
        }

        // Paso 3: Si es residente, sumar minutos y total a pagar en PagosResidentes
        if (esResidente) {
            const [estancia] = await pool.query('SELECT TIMESTAMPDIFF(MINUTE, hora_entrada, CURRENT_TIMESTAMP) AS minutos, total_a_pagar FROM Estancias WHERE id = ?', [id]);
            const { minutos, total_a_pagar } = estancia[0];

            await pool.query(`
                UPDATE PagosResidentes 
                SET minutos_acumulados = minutos_acumulados + ?, 
                    total_a_pagar = total_a_pagar + ?
                WHERE vehiculo_id = (
                    SELECT vehiculo_id FROM Estancias WHERE id = ?
                )`, [minutos, total_a_pagar, id]);
        }

        // Obtener el total a pagar actualizado para esta estancia
        const [updatedRows] = await pool.query('SELECT * FROM Estancias WHERE id = ?', [id]);
        if (updatedRows.length === 0) {
            return res.status(404).json({ message: "Error al obtener la estancia actualizada." });
        }

        res.status(200).json({ 
            message: "Estancia actualizada con éxito.", 
            estanciaActualizada: updatedRows[0]
        });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: "Error al actualizar la estancia." });
    }
};


export const comienzaMes = async (req, res) => {
    const { key } = req.body;

    if (!key || !(key == API_KEY_ADMIN)) {
        return res.status(403).json({ message: "Acceso no autorizado." });
    }

    try {
        // Paso 1: Eliminar las estancias de vehículos oficiales
        await pool.query(`
            DELETE Estancias 
            FROM Estancias 
            INNER JOIN Vehiculos ON Estancias.vehiculo_id = Vehiculos.id
            WHERE Vehiculos.tipo_vehiculo_id = 1
        `);

        // Paso 2: Reiniciar los registros de PagosResidentes
        await pool.query(`
            UPDATE PagosResidentes
            SET minutos_acumulados = 0, total_a_pagar = 0
        `);

        res.status(200).json({ message: "Operación 'Comienza Mes' completada con éxito." });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: "Error al realizar la operación 'Comienza Mes'." });
    }
};


// Función para eliminar una estancia
export const deleteEstancia = async (req, res) => {
    const { id } = req.params;
    const { key } = req.body;
    if (!key || !(key == API_KEY_ADMIN)) {
        return res.status(403).json({ message: "Acceso no autorizado." });
    }

    try {
        const result = await pool.query('DELETE FROM Estancias WHERE id = ?', [id]);

        if (result.affectedRows === 0) {
            return res.status(404).json({ message: "Estancia no encontrada." });
        }

        res.status(200).json({ message: "Estancia eliminada con éxito." });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: "Error al eliminar la estancia." });
    }
};
export const getAllPagosResidentes = async (req, res) => {
    try {
        const { key } = req.body;
        if (!key || !(key == API_KEY_ADMIN)) {
            return res.status(403).json({ message: "Acceso no autorizado." });
        }

        const [rows] = await pool.query('SELECT * FROM PagosResidentes');

        if (rows.length <= 0) {
            return res.status(404).json({ message: "No se encontraron pagos pendientes." });
        }

        res.status(200).json(rows);
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: "Error al obtener las estancias." });
    }
};

export const getEstanciasActivas = async (req, res) => {
    const { key } = req.body;

    if (!key || !(key == API_KEY_ADMIN)) {
        return res.status(403).json({ message: "Acceso no autorizado." });
    }

    try {
        const [rows] = await pool.query('SELECT * FROM Estancias WHERE hora_salida IS NULL');

        if (rows.length <= 0) {
            return res.status(404).json({ message: "No se encontraron estancias activas." });
        }

        res.status(200).json(rows);
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: "Error al obtener las estancias activas." });
    }
};

