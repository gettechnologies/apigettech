import { Router } from "express";
import { createTipoVehiculo, deleteTipoVehiculo, getTiposVehiculos, updateTipoVehiculo } from "../controllers/tipo.js";

const router = Router();




// Ruta para crear un nuevo tipo de vehículo
router.post('/createTipoVehiculo', createTipoVehiculo);

// Ruta para obtener todos los tipos de vehículos
router.post('/getTiposVehiculos', getTiposVehiculos);

// Ruta para actualizar un tipo de vehículo por ID
router.post('/updateTipoVehiculo/:id', updateTipoVehiculo);

// Ruta para eliminar un tipo de vehículo por ID
router.post('/deleteTipoVehiculo/:id',deleteTipoVehiculo);

export default router;
