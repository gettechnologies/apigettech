import { Router } from "express";
import { createVehiculo, getVehiculo, getVehiculos, updateVehiculo } from "../controllers/vehiculosController.js";
const router = Router();




// Ruta para crear un nuevo vehículo
router.post('/createVehiculo', createVehiculo);

// Ruta para obtener todos los vehículos
router.post('/getVehiculos', getVehiculos);

// Ruta para obtener un vehículo específico por ID
router.post('/getVehiculo/:id', getVehiculo);

// Ruta para actualizar un vehículo por ID
router.post('/updateVehiculo/:id', updateVehiculo);

export default router;
