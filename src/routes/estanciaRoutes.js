
import { Router } from "express";
const router = Router();

import {comienzaMes, createEstancia, deleteEstancia, getAllPagosResidentes, getEstancias, getEstanciasActivas, updateEstancia} from'../controllers/estanciaController.js';


// Ruta para crear una nueva estancia
router.post('/createEstancia', createEstancia);

// Ruta para obtener todas las estancias
router.post('/getEstancias', getEstancias);

// Ruta para actualizar una estancia (registrar la salida de un vehículo)
router.post('/updateEstancia/:id', updateEstancia);

// Ruta para eliminar una estancia
router.post('/deleteEstancia/:id', deleteEstancia);

// Ruta para iniciar el procedimiento "Comienza Mes"
router.post('/comienzaMes', comienzaMes);

// Ruta para obtener todas las estancias activas (sin hora de salida)
router.post('/getEstanciasActivas', getEstanciasActivas);

// Ruta para obtener todos los pagos de residentes
router.post('/getAllPagosResidentes', getAllPagosResidentes);

export default router;