import {pool} from '../connection.js'

async function queryWithRetry(query, retries = 5) {
    for (let i = 0; i < retries; i++) {
        try {
            const [rows] = await pool.query(query);
            return rows;
        } catch (error) {
            if ((error.code === 'ECONNRESET' || error.code === 'ETIMEDOUT') && i < retries - 1) {
                console.log(`Reintento ${i + 1} de ${retries}`);
                // Espera un breve momento antes de reintentar
                await new Promise(resolve => setTimeout(resolve, 1000 * (i + 1)));
                continue;
            }
            throw error;
        }
    }
}
export const getAllData = async (table) =>{
    try{
        // const [rows] = await pool.query(`SELECT * FROM ${table}`);
        const query = `SELECT * FROM ${table}`;
        const rows = await queryWithRetry(query);
        return rows;
    }catch(error){
        console.log(error);
        throw error;
        // return;
        /*return res.status(500).json({
            message : "Error algo salio mal"
        });*/
    }
}

export const getAllEnableData = async (table) =>{
    try{
        // const [rows] = await pool.query(`SELECT * FROM ${table} WHERE estatus = 1`);
        const query = `SELECT * FROM ${table} WHERE estatus = 1`;
        const rows = await queryWithRetry(query);
        return rows;
    }catch(error){
        // return res.status(500).json({
        //     message : "Error algo salio mal"
        // });
        console.log(error);
        throw error;
    }
}