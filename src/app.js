import express from 'express';
import multer from 'multer';
import path from 'path';
import { fileURLToPath } from 'url';
import { dirname } from 'path';

import estanciaRoutes from './routes/estanciaRoutes.js';
import vehiculoRoutes from './routes/vehiculosRoutes.js';
import tipoVehiculoRoutes from './routes/tipoVehiculoRoutes.js';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const app = express();

app.use(express.static(path.join(__dirname, 'public')));

app.use(express.json());



//* Rutas
app.use('/api/estancias', estanciaRoutes);
app.use('/api/vehiculos', vehiculoRoutes);
app.use('/api/tiposVehiculos', tipoVehiculoRoutes);

app.use((req, res, next) => {
    res.status(404).json({
        message: "Ruta no encontrada"
    });
});

export const API_KEY_ADMIN = 'passAdmin';
export const API_KEY_USER = 'passEstacionamiento';

//Exportando el modulo de la app
export default app;